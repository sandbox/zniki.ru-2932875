(function ($, window, Drupal, drupalSettings) {
  Drupal.geolocationYmapWidget = Drupal.geolocationYmapWidget || {

    /**
     * Init widget.
     */
    initWidget: function (e, map, settings) {
      if (settings.editable) {

        Drupal.geolocationYmapWidget.addPlacermarkFromWidgetValues(map);
        Drupal.geolocationYmapWidget.addEditButtons(map, settings.objectTypes);
        map.geoObjects.events.add('remove', Drupal.geolocationYmapWidget.objectsChangeHandler);
        map.events.add('click', Drupal.geolocationYmapWidget.mapClickHandler);

        // Select default edit button
        if (settings.selectedControl) {
          var editButton = Drupal.geolocationYmapWidget.getEditButton(map, settings.selectedControl);
          editButton.events.fire('click');
          editButton.select();
        }
      }
    },

    /**
     * Add edit buttons
     */
    addEditButtons: function (map, buttonTypes) {
      if (!buttonTypes) {
        buttonTypes = ['point'];
      }


      var buttonTitles = {
        // line:    Drupal.t('Add line', {}, {context: 'Geometry'}),
        // polygon: Drupal.t('Add polygone', {}, {context: 'Geometry'}),
        point:   Drupal.t('Add point', {}, {context: 'Geometry'})
      };

      $.each(buttonTypes, function (index, buttonType) {
        var button = new ymaps.control.Button({
          data: {
            image: drupalSettings.path.baseUrl + drupalSettings.geolocationYmap.modulePath + '/assets/images/icon-' + buttonType + '.png',
            title: buttonTitles[buttonType],
            editButtonType: buttonType // Custom data property
          }
        });
        button.events.add('click', Drupal.geolocationYmapWidget.editButtonClickHandler);
        map.controls.add(button);
      });
    },

    /**
     * Return selected edit button
     */
    getSelectedEditButton: function (map) {
      var button = null;
      map.controls.each(function (control) {
        if (control.data.get('editButtonType') && control.state.get('selected')) {
          button = control;
        }
      });
      return button;
    },

    /**
     * Return edit button by type
     */
    getEditButton: function (map, editButtonType) {
      var editButton;
      map.controls.each(function (control) {
        if (control.data.get('editButtonType') == editButtonType) {
          editButton = control;
        }
      });
      return editButton;
    },

    /**
     * Deselect controls
     */
    deselectControls: function (map) {
      // Deselect edit buttons
      map.controls.each(function (control) {
        if (control.data.get('editButtonType')) {
          control.deselect();
        }
      });

      // Deselect ruler
      var rulerControl = map.controls.get('rulerControl');
      if (rulerControl) {
        rulerControl.deselect();
      }
    },

    /**
     * Add event listeners.
     */
    onObjectAdded: function (e, map, object, geoQueryResult) {
      // Enable edit mode
      geoQueryResult.addEvents(['mapchange', 'editorstatechange', 'dragend', 'geometrychange'], Drupal.geolocationYmapWidget.objectsChangeHandler);
      geoQueryResult.addEvents('editorstatechange', Drupal.geolocationYmapWidget.objectEditorStateChangeHandler);
      geoQueryResult.addEvents('dblclick', Drupal.geolocationYmapWidget.objectsDblclickHandler);
      geoQueryResult.setOptions({draggable: true});
      geoQueryResult.each(function (object) {
        object.editor.startEditing();
      });
    },

    /**
     * Return total bounds by bounds collection
     */
    getTotalBounds: function (boundsCollection) {
      var totalBounds;

      $.each(boundsCollection, function (index, bounds) {
        if (!bounds) {
          return;
        }
        if (!totalBounds) {
          totalBounds = bounds;
          return;
        }

        // Min
        if (totalBounds[0][0] > bounds[0][0]) totalBounds[0][0] = bounds[0][0];
        if (totalBounds[0][1] > bounds[0][1]) totalBounds[0][1] = bounds[0][1];
        // Max
        if (totalBounds[1][0] < bounds[1][0]) totalBounds[1][0] = bounds[1][0];
        if (totalBounds[1][1] < bounds[1][1]) totalBounds[1][1] = bounds[1][1];
      });

      return totalBounds;
    },

    /**
     * Change map cursor
     */
    changeCursor: function (map, cursor) {
      var cursorAccessor = Drupal.geolocationYmap.data[map.mapId].cursor;
      if (!cursorAccessor || !cursorAccessor.getKey(cursor)) {
        cursorAccessor = map.cursors.push(cursor);
      }
      else {
        cursorAccessor.setKey(cursor);
      }
    },

    /**
     * Edit button click handler
     */
    editButtonClickHandler: function (event) {
      var button = event.get('target');
      var map = button.getMap();

      if (!button.state.get('selected')) {
        Drupal.geolocationYmapWidget.deselectControls(map);
        Drupal.geolocationYmapWidget.changeCursor(map, 'arrow');
      }
      else {
        Drupal.geolocationYmapWidget.changeCursor(map, 'grab');
      }
    },

    /**
     * Object editorstatechange handler
     */
    objectEditorStateChangeHandler: function (event) {
      var object = event.get('target');
      var map = object.getMap();
      if (map) {
        Drupal.geolocationYmap.data[map.mapId].drawingMode = object.editor.state.get('drawing');
      }
    },

    /**
     * Objects change handler
     */
    objectsChangeHandler: function (event) {
      var object = event.get('target');
      var map = object.getMap();
      if (map) {
        map.geoObjects.each(function (geoObject) {
          if (geoObject.geometry.getType() === 'Point') {
            Drupal.geolocationYmapWidget.updateCoordsFields(map, geoObject.geometry.getCoordinates());
          }
        });
      }
    },

    /**
     * Object dblclick handler
     */
    objectsDblclickHandler: function (event) {
      var object = event.get('target');
      var map = object.getMap();
      map.geoObjects.remove(object);
      event.stopPropagation();
    },

    /**
     * Map click handler
     */
    mapClickHandler: function (event) {
      var map = event.get('target');
      var settings = Drupal.geolocationYmap.data[map.mapId].settings;
      var selectedButton = Drupal.geolocationYmapWidget.getSelectedEditButton(map);

      if (selectedButton && !Drupal.geolocationYmap.data[map.mapId].drawingMode) {
        if (!settings.multiple) {
          map.geoObjects.removeAll();
        }

        var selectedButtonType = selectedButton.data.get('editButtonType');
        var geometry = event.get('coords');
        var startDrawing = false;

        if (selectedButtonType == 'line') {
          geometry = [geometry];
          startDrawing = true;
        }
        else if (selectedButtonType == 'polygon') {
          geometry = [[geometry]];
          startDrawing = true;
        }

        Drupal.geolocationYmap.addObjectByType(map, selectedButtonType, geometry, startDrawing);
      }
    },

    /**
     * Map boundschange handler
     */
    mapBoundschangeHandler: function (event) {
      var map = event.get('target');
      if (map) {
        /*
        var settings = Drupal.geolocationYmap.data[map.mapId].settings;

        if (settings.editable) {
          $('#' + map.mapId + ' ~ input[name$="[center]"]').val(map.getCenter().join(','));
          $('#' + map.mapId + ' ~ input[name$="[zoom]"]').val(map.getZoom());
        }

        if (settings.saveState) {
          Drupal.geolocationYmapWidget.saveMapStateToCookie(map.mapId, {
            center: map.getCenter(),
            zoom: map.getZoom()
          });
        }
        */
      }
    },

    /**
     * Set widget input values from map data.
     *
     * @param map
     * @param geometry {array}
     */
    updateCoordsFields: function (map, geometry) {
      if (map && geometry) {
        var $lat = this.getLatInput(map.mapId),
            $lng = this.getLngInput(map.mapId);
        if ($lat && $lng) {
          $lat.val(geometry[0]).trigger('change');
          $lng.val(geometry[1]).trigger('change');
        }
      }
    },

    /**
     * Get widget input values.
     *
     * @param map
     * @returns {array|false}
     */
    getCoordsValues: function (map) {
      var geometry = false;
      if (map) {
        var $lat = this.getLatInput(map.mapId),
            $lng = this.getLngInput(map.mapId);
        if ($lat && $lng) {
          if ($lat.val().length > 0 && $lng.val().length > 0) {
            geometry = [$lat.val(), $lng.val()];
          }
        }
      }
      return geometry;
    },

    /**
     * Get jQuery latitude input.
     * @param mapId
     * @returns {jQuery|false}
     */
    getLatInput: function(mapId) {
      var $input = $('input.' + mapId + '-lat');
      if ($input.length === 1) {
        return $input;
      }
      return false;
    },

    /**
     * Get jQuery longitude input.
     * @param mapId
     * @returns {jQuery|false}
     */
    getLngInput: function(mapId) {
      var $input = $('input.' + mapId + '-lng');
      if ($input.length === 1) {
        return $input;
      }
      return false;
    },

    /**
     * Add objects to map from widget input value.
     * @param map
     */
    addPlacermarkFromWidgetValues: function (map) {
      if (map) {
        var objectType = 'point',
            geometry = this.getCoordsValues(map),
            editMode = true,
            startDrawing = false;
        if (geometry) {
          Drupal.geolocationYmap.addObjectByType(map, objectType, geometry, editMode, startDrawing);
          Drupal.geolocationYmap.autoCentering(map);
        }
      }
    }

  };

  // Start widget when map is created.
  $(document).on('yandexMapInit', Drupal.geolocationYmapWidget.initWidget);

  //
  $(document).on('objectAdded', Drupal.geolocationYmapWidget.onObjectAdded);

})(jQuery, window, Drupal, drupalSettings);
