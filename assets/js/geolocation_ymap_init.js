(function ($, window, Drupal, drupalSettings) {
  Drupal.behaviors.geolocationYmap = {
    attach: function (context, drupalSettings) {
      $('div.geolocation-ymap-canvas', context).once('geolocation-ymap-canvas--started').each(function() {
        var mapId = this.id;
        if (!mapId) {
          mapId = this.id = Drupal.geolocationYmap.generateMapId();
        }
        ymaps.ready(function () {
          Drupal.geolocationYmap.mapInit(mapId);
        });
      });
    }
  };

  Drupal.geolocationYmap = Drupal.geolocationYmap || {
    data: {},

    defaultSettings: {
      center: [0, 0],
      zoom: 0
    },

    /**
     * Initialize map
     */
    mapInit: function (mapId, settings) {
      var dataSettings = Drupal.geolocationYmap.getDataSettings(mapId);
      settings = $.extend({}, Drupal.geolocationYmap.defaultSettings, dataSettings, settings);

      // Create map
      var map = Drupal.geolocationYmap.createMap(mapId, settings);

      // Set geo objects global options
      var globalOptions = {strokeWidth: 4};
      /*
      if (drupalSettings.geolocationYmap.preset) {
        globalOptions.preset = drupalSettings.geolocationYmap.preset;
      }
      if (settings.objectPreset) {
        globalOptions.preset = settings.objectPreset;
      }
      */
      map.geoObjects.options.set(globalOptions);

      // Add geo objects
      if (!settings.withoutObjects) {
        if (settings.objects) {
          Drupal.geolocationYmap.addObject(map, settings.objects, settings.clusterize);
        }
      }

      // Auto centering
      if (settings.autoCentering) {
        Drupal.geolocationYmap.autoCentering(map);
      }

      // Auto zooming
      if (settings.autoZooming) {
        Drupal.geolocationYmap.autoZooming(map);
      }

      // Tweak search control
      // Drupal.geolocationYmap.tweakSearchControl(map);

      $('#' + mapId).trigger('yandexMapInit', [map, settings]);

      return map;
    },

    /**
     * Create map
     */
    createMap: function (mapId, settings) {
      var mapState = Drupal.geolocationYmap.getMapStateFromSettings(settings);
      var $mapContainter = $('#' + mapId);

      // Set the container size.
      $mapContainter.css({
        height: settings.height,
        width: settings.width
      });
      var map = new ymaps.Map(mapId, mapState, settings.options);
      map.mapId = mapId;

      Drupal.geolocationYmap.data[mapId] = {
        map: map,
        settings: settings,
        cursor: null,
        drawingMode: false
      };

      return map;
    },

    /**
     * Return map data-* settings
     */
    getDataSettings: function (mapId) {
      var $map = $('#' + mapId);
      var settings = {};
      var arrayTypeSettings = ['center', 'controls', 'behaviors'];

      if (drupalSettings.geolocationYmap.hasOwnProperty(mapId)) {
        settings = $.extend({}, drupalSettings.geolocationYmap[mapId].settings);
      }

      $.each($map.data(), function (attributeKey, attributeValue) {
        var matches = attributeKey.match(/map(.+)/);
        if (matches) {
          var settingKey = matches[1].substring(0, 1).toLowerCase() + matches[1].substring(1);
          if ($.inArray(settingKey, arrayTypeSettings) != -1 && $.type(attributeValue) != 'array') {
            attributeValue = (attributeValue == '<none>') ? [] : attributeValue.split(',');
          }
          settings[settingKey] = attributeValue;
        }
      });

      return settings;
    },

    /**
     * Return default map state from settings
     */
    getMapStateFromSettings: function (settings) {
      var state = {};
      $.each(['behaviors', 'bounds', 'center', 'controls', 'type', 'zoom'], function (index, stateKey) {
        if (stateKey in settings) {
          state[stateKey] = settings[stateKey];
        }
      });
      return state;
    },

    /**
     * Add geo object
     */
    addObject: function (map, object, clusterize) {
      if (!object) return;

      if ($.type(object) === 'string')  {
        object = JSON.parse(object);
      }

      var geoQueryResult = ymaps.geoQuery(object);

      // Clusterize placemarks
      if (clusterize) {
        var points = geoQueryResult.search('geometry.type = "Point"');
        var notPoints = geoQueryResult.search('geometry.type != "Point"');
        var clusterer = points.clusterize({
          hasHint: false,
          margin: 15,
          zoomMargin: 20
        });
        map.geoObjects.add(clusterer);
        notPoints.addToMap(map);
      }
      else {
        geoQueryResult.addToMap(map);
      }

      $('#' + map.mapId).trigger('objectAdded', [map, object, geoQueryResult]);
    },

    /**
     * Add geo object by type
     */
    addObjectByType: function (map, objectType, geometry, startDrawing) {
      if (objectType == 'point') {
        var object = new ymaps.Placemark(geometry);
      }
      else if (objectType == 'line') {
        var object = new ymaps.Polyline(geometry);
      }
      else if (objectType == 'polygon') {
        var object = new ymaps.Polygon(geometry);
      }
      Drupal.geolocationYmap.addObject(map, object);

      if (startDrawing) {
        object.editor.startDrawing();
      }
    },

    /**
     * Return map.geoObjects in GeoJSON format
     */
    getObjectsInGeoJson: function (map) {
      var objects = {
        type: 'FeatureCollection',
        features: []
      };
      map.geoObjects.each(function (object) {
        var feature = {
          type: 'Feature',
          geometry: {
            type: object.geometry.getType(),
            coordinates: object.geometry.getCoordinates()
          }
        };
        objects.features.push(feature);
      });
      return JSON.stringify(objects);
    },

    /**
     * Auto centering map
     */
    autoCentering: function (map) {
      if (map.geoObjects.getLength() === 0) return;
      var centerAndZoom = ymaps.util.bounds.getCenterAndZoom(map.geoObjects.getBounds(), map.container.getSize());
      map.setCenter(centerAndZoom.center);
    },

    /**
     * Auto zooming map
     */
    autoZooming: function (map) {
      if (map.geoObjects.getLength() === 0) return;
      var mapSize = map.container.getSize();
      var centerAndZoom = ymaps.util.bounds.getCenterAndZoom(
        map.geoObjects.getBounds(),
        mapSize,
        ymaps.projection.wgs84Mercator,
        {margin: 30}
      );
      map.setZoom(centerAndZoom.zoom <= 16 ? centerAndZoom.zoom : 16);
    },

    /**
     * Return new map id
     */
    generateMapId: function (number) {
      if (!number) number = 1;
      var mapId = 'geolocation-ymap-' + number;
      if ($('#' + mapId).length > 0) {
        return Drupal.geolocationYmap.generateMapId(number + 1);
      }
      return mapId;
    },

    /**
     * Return total bounds by bounds collection
     */
    getTotalBounds: function (boundsCollection) {
      var totalBounds;

      $.each(boundsCollection, function (index, bounds) {
        if (!bounds) {
          return;
        }
        if (!totalBounds) {
          totalBounds = bounds;
          return;
        }

        // Min
        if (totalBounds[0][0] > bounds[0][0]) totalBounds[0][0] = bounds[0][0];
        if (totalBounds[0][1] > bounds[0][1]) totalBounds[0][1] = bounds[0][1];
        // Max
        if (totalBounds[1][0] < bounds[1][0]) totalBounds[1][0] = bounds[1][0];
        if (totalBounds[1][1] < bounds[1][1]) totalBounds[1][1] = bounds[1][1];
      });

      return totalBounds;
    }

    /**
     * Tweak search control
     */
    /*
    tweakSearchControl: function (map) {
      var searchControl = map.controls.get('searchControl');
      if (searchControl) {
        // Remove search result placemark after close balloon
        searchControl.events.add('resultshow', function (event) {
          var resultIndex = event.get('index');
          var result = searchControl.getResultsArray()[resultIndex];
          result.balloon.events.add('close', function (event) {
            result.getParent().remove(result);
          });
        });
      }
    },
    */

  };
})(jQuery, window, Drupal, drupalSettings);
