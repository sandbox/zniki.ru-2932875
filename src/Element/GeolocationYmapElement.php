<?php

namespace Drupal\geolocation_ymap\Element;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Component\Utility\Html;
use Drupal\geolocation_ymap\YandexMapDisplayTrait;


/**
 * Provides a render element to display a geolocation Yandex map.
 *
 * Usage example:
 * @code
 * $elements['map'] = [
 *   '#type' => 'geolocation_yandex_map',
 *   '#id' => 'uniqid',
 *   '#ymap_settings' => [
 *     'editable' => FALSE,
 *     'autoZooming' => FALSE,
 *     'autoCentering' => FALSE,
 *     'height' => '400px',
 *     'width' => '100%',
 *     'behaviors' => ['default' => 'default'],
 *     'center' => '55.75378270800939,37.62221595263608', // Moscow.
 *     'controls' => ['trafficControl', 'zoomControl', 'typeSelector'],
 *     'type' => yandex#hybrid,
 *     'zoom' => 10,
 *   ],
 *   '#features' => [
 *     ['type' => 'Point', 'coordinates'=> [60, 40]],
 *     ['type' => 'Point', 'coordinates'=> [50, 40]],
 *     ['type' => 'Point', 'coordinates'=> [30, 40]],
 *   ],
 * ];
 * @endcode
 *
 * @RenderElement("geolocation_yandex_map")
 */
class GeolocationYmapElement extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    $info = [
      '#process' => [
        [$class, 'processGroup'],
      ],
      '#pre_render' => [
        [$class, 'preRenderGroup'],
        [$this, 'preRenderMap'],
      ],
      '#id' => NULL,
      '#theme' => 'geolocation_ymap_wrapper',
      '#ymap_settings' => [],
      '#features' => [],
      '#children' => [],
    ];

    return $info;
  }

  /**
   * Map element.
   *
   * @param array $element
   *   Element.
   *
   * @return array
   *   Renderable map.
   */
  public function preRenderMap(array $element) {
    if (!empty($element['#id'])) {
      $canvas_id = $element['#id'];
    }
    else {
      $canvas_id = Html::getUniqueId('geolocation_ymap');
    }

    $attributes = new Attribute();
    if (!empty($element['#attributes'])) {
      $attributes = new Attribute($element['#attributes']);
    }
    $attributes->setAttribute('id', $canvas_id);
    $element['#attributes'] = $attributes;

    // Map JS settings.
    $settings = [];
    if (!empty($element['#ymap_settings']) && is_array($element['#ymap_settings'])) {
      $settings['yandex_map_settings'] = $element['#ymap_settings'];
    }

    $settings = YandexMapDisplayTrait::getYandexMapSettings($settings);
    $settings = YandexMapDisplayTrait::preprocessYandexMapJsSettings($settings);
    $yandex_map_settings = $settings['yandex_map_settings'];

    // Add objects to Map.
    if (!empty($element['#features']) && is_array($element['#features'])) {
      $featureCollection = new YandexMapFeatureCollection;
      foreach ($element['#features'] as $feature) {
        if (!empty($feature['type']) && !empty($feature['coordinates'])) {
          $featureCollection->addFeature($feature['type'], $feature['coordinates']);
        }
      }
      $yandex_map_settings['withoutObjects'] = FALSE;
      $yandex_map_settings['objects'] = $featureCollection->getFeatureCollection();
    }

    $default_attached = [
      '#attached' => [
        'library' => ['geolocation_ymap/geolocation_ymap.ymap_init'],
        'drupalSettings' => [
          'geolocationYmap' => [
            $canvas_id => [
              'settings' => $yandex_map_settings,
            ],
            'modulePath' => drupal_get_path('module', 'geolocation_ymap'),
          ],
        ],
      ],
    ];
    $element = BubbleableMetadata::mergeAttachments($default_attached, $element);

    foreach (Element::children($element) as $child) {
      $element['#children'][] = $child;
    }

    if (!empty($element['#children'])) {
      uasort($element['#children'], [
        SortArray::class,
        'sortByWeightProperty',
      ]);
    }

    return $element;
  }
}