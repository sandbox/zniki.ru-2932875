<?php

namespace Drupal\geolocation_ymap;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\Html;

/**
 * Class YandexMapDisplayTrait.
 *
 * @package Drupal\geolocation_ymap
 */
trait YandexMapDisplayTrait {

  /**
   * Yandex map style - Roadmap.
   *
   * @var string
   */
  public static $ROADMAP = 'yandex#map';

  /**
   * Yandex map style - Satellite.
   *
   * @var string
   */
  public static $SATELLITE = 'yandex#satellite';

  /**
   * Yandex map style - Hybrid.
   *
   * @var string
   */
  public static $HYBRID = 'yandex#hybrid';

  /**
   * Yandex map max zoom level.
   *
   * @var int \Drupal::translation
   */
  public static $MAXZOOMLEVEL = 23;

  /**
   * Yandex map min zoom level.
   *
   * @var int
   */
  public static $MINZOOMLEVEL = 0;

  /**
   * Control's custom set name.
   *
   * @var int
   */
  public static $CONTROLSCUSTOM = 'controls_custom';

  /**
   * An array of all available map types.
   *
   * @return array
   *   The map types.
   */
  public static function getMapTypes() {
    $mapTypes = [
      static::$ROADMAP   => 'Road map view',
      static::$SATELLITE => 'Satellite images',
      static::$HYBRID    => 'A mixture of normal and satellite views',
    ];

    return array_map('t', $mapTypes);
  }

  /**
   * An array of all available map behaviors.
   * @see https://tech.yandex.com/maps/doc/jsapi/2.1/ref/reference/map.behavior.Manager-docpage/
   *
   * @return array
   */
  public static function getMapBehaviors() {
    $mapBehaviors = [
      'default' => 'default - Shortcut for enabling/disabling default map behaviors.',
      'drag' => 'drag - Dragging the map when the left mouse button is held down, or by a single touch.',
      'scrollZoom' => 'scrollZoom - Changing the zoom with the mouse wheel.',
      'dblClickZoom' => 'dblClickZoom - Zooming the map on a double click.',
      'multiTouch' => 'multiTouch -Zooming the map with a multi touch (i.e. on a touch screen).',
      'rightMouseButtonMagnifier' => 'rightMouseButtonMagnifier - Magnifying the area that is selected using the right mouse button (for desktop browsers only).',
      'leftMouseButtonMagnifier' => 'leftMouseButtonMagnifier - Magnifying the area selected by the left mouse button or a single touch.',
      'ruler' => 'ruler - Measuring distance.',
      'routeEditor' => 'routeEditor - Route editor.',
    ];

    return array_map('t', $mapBehaviors);
  }

  /**
   * An array of all available predefined sets of controls.
   * @see https://tech.yandex.com/maps/doc/jsapi/2.1/ref/reference/control.Manager-docpage/
   * @see YandexMapsDisplayTrait::getMapControl()
   *
   * @return array
   */
  public static function getMapControlsSets() {
    $mapControlsSets = [
      static::$CONTROLSCUSTOM => 'custom - Use "Map custom controls" for attach.',
      'smallMapDefaultSet' => 'smallMapDefaultSet - Basic set of controls, optimized for small maps and mobile phone screens. It includes the following controls: "zoomControl", "searchControl", "typeSelector", "geolocationControl" and "fullscreenControl". All controls in this set are minimized to buttons with icons.',
      'mediumMapDefaultSet' => 'mediumMapDefaultSet - Basic set of controls, optimized for medium-sized maps and tablet screens. In addition to the basic set of controls (see above), this set includes "rulerControl" and "trafficControl".',
      'largeMapDefaultSet' => 'largeMapDefaultSet - Basic set of controls, optimized for large maps and desktops. The "routeEditor" control is added to the set and the "fullScreen" control is removed from the set, compared to the "mediumMapDefaultSet".',
    ];

    return array_map('t', $mapControlsSets);
  }

  /**
   * An array of all available map controls.
   * @see https://tech.yandex.com/maps/doc/jsapi/2.1/ref/reference/control.Manager-docpage/
   *
   * @return array
   */
  public static function getMapControls() {
    $mapControls = [
      'fullscreenControl' => 'fullscreenControl - Button for opening the map in full-screen mode.',
      'geolocationControl' => 'geolocationControl - Button for defining the user location.',
      'routeEditor' => 'routeEditor - Button for enabling or disabling the behavior "route editor".',
      'rulerControl' => 'rulerControl - Button for enabling or disabling the behavior "ruler".',
      'searchControl' => "searchControl - Search box for processing the user's search query.",
      'trafficControl' => 'trafficControl - Traffic panel.',
      'typeSelector' => 'typeSelector - Panel for selecting the map type.',
      'zoomControl' => 'zoomControl - Zoom slider.',
    ];

    return array_map('t', $mapControls);
  }

  /**
   * Provide a populated settings array.
   * @see https://tech.yandex.com/maps/doc/jsapi/2.1/ref/reference/Map-docpage/
   *
   * @return array
   *   The settings array with the default map settings.
   */
  public static function getYandexMapDefaultSettings() {
    return [
      'yandex_map_settings' => [
        'editable' => FALSE,
        'autoZooming' => FALSE,
        'autoCentering' => FALSE,
        'height' => '400px',
        'width' => '100%',
        // state:
        'behaviors' => ['default' => 'default'],
        // 'bounds' => ,
        'center' => '55.75378270800939,37.62221595263608', // Moscow.
        'controls' => [],
        'controls_set' => 'mediumMapDefaultSet',
        // 'margin' => ,
        'type' => static::$ROADMAP,
        'zoom' => 10,
        'options' => [
          'autoFitToViewport' => 'ifNull',
          'avoidFractionalZoom' => TRUE,
          'exitFullscreenByEsc' => TRUE,
          'fullscreenZIndex' => 10000,
          'mapAutoFocus' => TRUE,
          'maxAnimationZoomDifference' => 5,
          'maxZoom' => static::$MAXZOOMLEVEL,
          'minZoom' => static::$MINZOOMLEVEL,
          'nativeFullscreen' => FALSE,
          // 'projection' => ,
          'restrictMapArea' => FALSE,
          'suppressMapOpenBlock' => FALSE,
          'suppressObsoleteBrowserNotifier' => FALSE,
          'yandexMapAutoSwitch' => TRUE,
          'yandexMapDisablePoiInteractivity' => FALSE,
        ],
      ],
    ];
  }

  /**
   * Provide a summary array to use in field formatters.
   *
   * @param array $settings
   *   The current map settings.
   *
   * @return array
   *   An array to use as field formatter summary.
   */
  public function getYandexMapSettingsSummary(array $settings) {
    $types = $this->getMapTypes();
    $summary = [];
    $summary[] = $this->t('Map Type: @type', ['@type' => $types[$settings['yandex_map_settings']['type']]]);
    $summary[] = $this->t('Zoom level: @zoom', ['@zoom' => $settings['yandex_map_settings']['zoom']]);
    $summary[] = $this->t('Height: @height', ['@height' => $settings['yandex_map_settings']['height']]);
    $summary[] = $this->t('Width: @width', ['@width' => $settings['yandex_map_settings']['width']]);
    return $summary;
  }

  /**
   * Provide a generic map settings form array.
   *
   * @param array $settings
   *   The current map settings.
   * @param string $form_prefix
   *   Form specific optional prefix.
   *
   * @return array
   *   A form array to be integrated in whatever.
   */
  public function getYandexMapSettingsForm(array $settings, $form_prefix = '') {
    $settings['yandex_map_settings'] += self::getYandexMapDefaultSettings()['yandex_map_settings'];
    $form = [
      'yandex_map_settings' => [
        '#type' => 'details',
        '#title' => t('Yandex Map settings'),
        '#description' => t('Additional map settings provided by Yandex Map'),
      ],
    ];
    $form_ya = &$form['yandex_map_settings'];
    $ya_settings = $settings['yandex_map_settings'];

    /*
     * General settings.
     */
    $form_ya['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Height'),
      '#description' => $this->t('Enter the dimensions and the measurement units. E.g. 200px or 100%.'),
      '#size' => 8,
      '#default_value' => $ya_settings['height'],
    ];
    $form_ya['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width'),
      '#description' => $this->t('Enter the dimensions and the measurement units. E.g. 200px or 100%.'),
      '#size' => 8,
      '#default_value' => $ya_settings['width'],
    ];

    /**
     * State.
     */
    $form_ya['behaviors'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Behavior'),
      '#options' => $this->getMapBehaviors(),
      '#default_value' => $ya_settings['behaviors'],
    ];
    $form_ya['autoCentering'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Map auto centering'),
      '#default_value' => $ya_settings['autoCentering'],
    ];
    $form_ya['center'] = [
      '#type' => 'textfield',
      '#title' => t('Map center'),
      '#description' => t('Map center coordinates: Longtitude,Latitude. Example: <code>37.62,55.75</code>'),
      '#default_value' => $ya_settings['center'],
    ];
    $form_ya['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Default map type'),
      '#options' => $this->getMapTypes(),
      '#default_value' => $ya_settings['type'],
    ];
    $form_ya['autoZooming'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Map auto zooming'),
      '#default_value' => $ya_settings['autoZooming'],
    ];
    $form_ya['zoom'] = [
      '#type' => 'select',
      '#title' => $this->t('Zoom level'),
      '#options' => range(static::$MINZOOMLEVEL, static::$MAXZOOMLEVEL),
      '#description' => $this->t('The initial resolution at which to display the map, where zoom 0 corresponds to a map of the Earth fully zoomed out, and higher zoom levels zoom in at a higher resolution.'),
      '#default_value' => $ya_settings['zoom'],
    ];

    /*
     * Control settings.
     */
    $controls_set_class = Html::getUniqueId('yandex-map-settings--controls');
    $form_ya['controls_set'] = [
      '#type' => 'radios',
      '#title' => $this->t('Map controls set'),
      '#options' => $this->getMapControlsSets(),
      '#attributes' => ['class' => [$controls_set_class]],
      '#default_value' => isset($ya_settings['controls_set']) ? $ya_settings['controls_set'] : FALSE,
    ];
    $form_ya['controls'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Map custom controls'),
      '#options' => $this->getMapControls(),
      '#default_value' => $ya_settings['controls'],
      '#states' => [
        'visible' => [':input.' . $controls_set_class => ['value' => static::$CONTROLSCUSTOM]],
      ],
    ];

    /**
     * Options.
     */
    $form_ya['options']['maxZoom'] = [
      '#type' => 'select',
      '#title' => $this->t('Max Zoom level'),
      '#options' => range(static::$MINZOOMLEVEL, static::$MAXZOOMLEVEL),
      '#description' => $this->t('The maximum zoom level which will be displayed on the map. If omitted, or set to null, the maximum zoom from the current map type is used instead.'),
      '#default_value' => $ya_settings['options']['maxZoom'],
    ];
    $form_ya['options']['minZoom'] = [
      '#type' => 'select',
      '#title' => $this->t('Min Zoom level'),
      '#options' => range(static::$MINZOOMLEVEL, static::$MAXZOOMLEVEL),
      '#description' => $this->t('The minimum zoom level which will be displayed on the map. If omitted, or set to null, the minimum zoom from the current map type is used instead.'),
      '#default_value' => $ya_settings['options']['minZoom'],
    ];

    return $form;
  }

  /**
   * Provide settings ready to handover to JS to feed to Yandex Maps.
   *
   * @param array $settings
   *   Current settings. Might contain unrelated settings as well.
   *
   * @return array
   *   An array only containing keys defined in this trait.
   */
  public static function getYandexMapSettings(array $settings = []) {
    $default_settings = self::getYandexMapDefaultSettings();
    $settings = array_replace_recursive($default_settings, $settings);
    // We want 'yandex_map_settings'.
    if (empty($settings['yandex_map_settings'])) {
      $settings = $default_settings;
    }
    // $settings = self::preprocessYandexMapJsSettings($settings);

    return $settings;
  }

  public static function preprocessYandexMapJsSettings(array $settings) {
    if (!isset($settings['yandex_map_settings'])) {
      return $settings;
    }

    $ya_settings = &$settings['yandex_map_settings'];
    // Remove controls_set settings.
    if (!empty($ya_settings['controls_set']) && $ya_settings['controls_set'] != static::$CONTROLSCUSTOM) {
      $ya_settings['controls'] = [$ya_settings['controls_set'] => $ya_settings['controls_set']];
    }
    unset($ya_settings['controls_set']);

    if (!empty($ya_settings['behaviors']) && is_array($ya_settings['behaviors'])) {
      $ya_settings['behaviors'] = array_keys(array_filter($ya_settings['behaviors']));
    }

    if (!empty($ya_settings['controls']) && is_array($ya_settings['controls'])) {
      $ya_settings['controls'] = array_keys(array_filter($ya_settings['controls']));
    }

    if (is_string($ya_settings['center']) && strpos($ya_settings['center'], ',') !== FALSE) {
      $ya_settings['center'] = explode(',' ,$ya_settings['center']);
    }

    return $settings;
  }

}
