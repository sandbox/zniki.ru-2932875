<?php

namespace Drupal\geolocation_ymap\Plugin\Field\FieldFormatter;

use Drupal\geolocation\Plugin\Field\FieldFormatter\GeolocationLatlngFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\geolocation_ymap\YandexMapDisplayTrait;
use Drupal\geolocation_ymap\YandexMapFeatureCollection;

/**
 * Plugin implementation of the 'geolocation_latlng' formatter.
 *
 * @FieldFormatter(
 *   id = "geolocation_ymap_formatter",
 *   label = @Translation("Geolocation Yandex Maps API"),
 *   field_types = {
 *     "geolocation"
 *   }
 * )
 */
class GeolocationYmapFormatter extends GeolocationLatlngFormatter {

  use YandexMapDisplayTrait;


  /**
   * Show each point on an individual map.
   *
   * @var string
   */
  public static $DEFAULT_MAP = 'default_map';

  /**
   * Show all points on one map.
   *
   * @var string
   */
  public static $COMMON_MAP = 'common_map';

  /**
   * An array of all available map formatters types.
   *
   * @return array
   *   The map types.
   */
  public static function getFormatterMapTypes() {
    $mapTypes = [
      static::$DEFAULT_MAP => 'Default map - Show each point on an individual map.',
      static::$COMMON_MAP => 'Common map - Show all points on one map.',
    ];

    return array_map('t', $mapTypes);
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $default_settings = [
      'formatterMapType' => static::$DEFAULT_MAP,
      'yandex_map_settings' => [
        'editable' => FALSE,
        'autoCentering' => TRUE,
        'autoZooming' => TRUE,
      ],
    ];
    $settings = array_replace_recursive(self::getYandexMapDefaultSettings(), parent::defaultSettings(), $default_settings);

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    $element = parent::settingsForm($form, $form_state);

    $element['formatterMapType'] = [
      '#type' => 'radios',
      '#title' => $this->t('Map type'),
      '#options' => $this->getFormatterMapTypes(),
      '#default_value' => isset($settings['formatterMapType']) ? $settings['formatterMapType'] : FALSE,
    ];

    $form_prefix = 'fields][' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][';
    $element += $this->getYandexMapSettingsForm($settings, $form_prefix);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();
    $summary = array_merge($summary, $this->getYandexMapSettingsSummary($settings));

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    if ($items->isEmpty()) {
      return [];
    }
    $settings = $this->getSettings();
    $elements = [];
    switch ($settings['formatterMapType']) {
      case static::$COMMON_MAP:
        $elements = $this->viewElementsCommonMap($items, $langcode);
        break;
      default:
        $elements = $this->viewElementsDefaultMap($items, $langcode);
        break;
    }
    return $elements;
  }


  /**
   * Show all points on one map.
   */
  public function viewElementsCommonMap(FieldItemListInterface $items, $langcode) {
    $featureCollection = new YandexMapFeatureCollection;
    $settings = $this->getYandexMapSettings($this->getSettings());
    $canvas_id = Html::getUniqueId($this->fieldDefinition->getName());

    // Add all Points to one feature collection.
    foreach ($items as $delta => $item) {
      if ($item->isEmpty()) {
        continue;
      }
      $coordinates = [
        $item->lat,
        $item->lng,
      ];
      $featureCollection->addPoint($coordinates);
    }


    $yandex_map_settings = $settings['yandex_map_settings'];
    $yandex_map_settings['withoutObjects'] = $items->isEmpty();
    $yandex_map_settings['objects'] = $featureCollection->getFeatureCollection();

    $elements = [];

    // Add only one map.
    $elements[0] = [
      '#type' => 'geolocation_yandex_map',
      '#id' => $canvas_id,
      '#ymap_settings' => $yandex_map_settings,
    ];

    return $elements;
  }

  /**
   * Show each point on an individual map.
   */
  public function viewElementsDefaultMap(FieldItemListInterface $items, $langcode) {
    $elements = [];
    // Put each point to individual element.
    foreach ($items as $delta => $item) {
      if ($item->isEmpty()) {
        continue;
      }
      $featureCollection = new YandexMapFeatureCollection;
      $canvas_id = Html::getUniqueId($this->fieldDefinition->getName());
      $settings = $this->getYandexMapSettings($this->getSettings());

      $coordinates = [
        $item->lat,
        $item->lng,
      ];
      $featureCollection->addPoint($coordinates);


      $yandex_map_settings = $settings['yandex_map_settings'];
      // Force to use this settings.
      $yandex_map_settings['withoutObjects'] = $item->isEmpty();
      $yandex_map_settings['objects'] = $featureCollection->getFeatureCollection();

      // Add the map container.
      $elements[$delta] = [
        '#type' => 'geolocation_yandex_map',
        '#id' => $canvas_id,
        '#ymap_settings' => $yandex_map_settings,
      ];
    }

    return $elements;
  }

}
