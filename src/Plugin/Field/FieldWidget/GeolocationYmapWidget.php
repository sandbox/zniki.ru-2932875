<?php

namespace Drupal\geolocation_ymap\Plugin\Field\FieldWidget;

use Drupal\geolocation\Plugin\Field\FieldWidget\GeolocationLatlngWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\geolocation\GeolocationCore;
use Drupal\Component\Utility\Html;
use Drupal\geolocation_ymap\YandexMapDisplayTrait;

/**
 * Plugin implementation of the 'geolocation_ymap_widget' widget.
 *
 * @FieldWidget(
 *   id = "geolocation_ymap_widget",
 *   label = @Translation("Geolocation Yandex Maps API"),
 *   field_types = {
 *     "geolocation"
 *   }
 * )
 */
class GeolocationYmapWidget extends GeolocationLatlngWidget {

  use YandexMapDisplayTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $default_settings = [
      'yandex_map_settings' => [
        'editable' => TRUE,
        'autoCentering' => TRUE,
        'autoZooming' => TRUE,
      ],
    ];

    $settings = array_replace_recursive(parent::defaultSettings(), self::getYandexMapDefaultSettings(), $default_settings);

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $canvas_id = Html::getId($this->fieldDefinition->id() . '-delta-' . $delta  . '-map-canvas');
    $settings = $this->getYandexMapSettings($this->getSettings());

    $yandex_map_settings = $settings['yandex_map_settings'];
    // Force to use this settings.
    $yandex_map_settings['editable'] = TRUE;
    $yandex_map_settings['withoutObjects'] = $items[$delta]->isEmpty();
    // @todo: support other objects.
    $yandex_map_settings['objectTypes'] = ['point'];

    // Add the map container.
    $element['map_canvas'] = [
      '#type' => 'geolocation_yandex_map',
      '#id' => $canvas_id,
      '#weight' => -1,
      '#ymap_settings' => $yandex_map_settings,
      '#attached' => [
        'library' => ['geolocation_ymap/geolocation_ymap.ymap_widget'],
      ],
    ];

    // Add classes to input fields to display Point on yMap from it's values.
    $element['lat']['#attributes']['class'][] = $canvas_id . '-lat';
    $element['lng']['#attributes']['class'][] = $canvas_id . '-lng';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    $element = parent::settingsForm($form, $form_state);

    $form_prefix = 'fields][' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][';
    $element += $this->getYandexMapSettingsForm($settings, $form_prefix);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();

    $summary = array_merge($summary, $this->getYandexMapSettingsSummary($settings));
    return $summary;
  }

}
