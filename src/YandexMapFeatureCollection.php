<?php

namespace Drupal\geolocation_ymap;

/**
 * Class for easy add object to map using Feature Collection.
 *
 * @see https://tech.yandex.com/maps/doc/jsapi/2.1/ref/reference/ObjectManager-docpage/#add-param-objects
 */
class YandexMapFeatureCollection {

  /**
   * @var array
   */
  protected $features;

  public function __construct() {
    $this->features = [];
  }

  /**
   * @param array $coordinates
   */
  public function addPoint(array $coordinates) {
    if (!empty($coordinates) && count($coordinates) == 2) {
      $this->addFeature('Point', $coordinates);
    }
  }

  /**
   * @param string $type
   * @param array $coordinates
   */
  protected function addFeature(string $type, array $coordinates) {
    if (in_array($type, $this->getAllowedTypes()) && !empty($coordinates)) {
      $new_feature = [
        'type' => 'Feature',
        'geometry' => [
          'type' => $type,
          'coordinates' => $coordinates,
        ],
      ];
      $this->features[] = $new_feature;
    }
  }

  /**
   * Get feature collection for Yandex objects.
   *
   * @return array
   */
  public function getFeatureCollection() {
    $featureCollection = [];
    if (!empty($this->features)) {
      $featureCollection = [
        'type' => 'FeatureCollection',
        // features must be array in JSON.
        'features' => array_values($this->features),
      ];
    }
    return $featureCollection;
  }

  /**
   * Allowed features type available to add to collection.
   * @return array
   */
  public static function getAllowedTypes() {
    $types = [
      'Point',
      'LineString',
      'Polygon',
    ];
    return $types;
  }

}